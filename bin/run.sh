#!/bin/bash -ex

remove_old_container() {
    local container=$1
    local running_ids="$(docker ps --quiet --filter name=$container)"
    [[ "$running_ids" ]] && docker stop $running_ids || true
    local ids="$(docker ps --all --quiet --filter name=$container)"
    [[ "$ids" ]] && docker rm $ids || true
}

run_container() {
    local image="$1"
    local container_name="$image"
    local host_port=80
    local container_server_port=5000
    local options=""
    options+=" --detach=true"
    options+=" --name $container_name"
    options+=" --expose $container_server_port"
    options+=" --publish $host_port:$container_server_port"
    remove_old_container $container_name
    docker run $options $image
}

run_container commuter_challenge_competition
