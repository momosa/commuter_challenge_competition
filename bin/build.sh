#!/bin/bash -ex

declare -r CCC="$(cd "${BASH_SOURCE%/*}/.."; pwd; cd - &> /dev/null)"

[[ "$COMMUTER_EMAIL" ]] || { echo "Define COMMUTER_EMAIL in the env"; exit 1; }
[[ "$COMMUTER_PASSWORD" ]] || { echo "Define COMMUTER_PASSWORD in the env"; exit 1; }

build_image() {
    docker build \
        --tag commuter_challenge_competition \
        --file "${CCC}/Dockerfile" \
        --build-arg COMMUTER_EMAIL=$COMMUTER_EMAIL \
        --build-arg COMMUTER_PASSWORD=$COMMUTER_PASSWORD \
        "${CCC}"
}

build_image
