# Commuter Challenge Competition

Is your organization participating in Ann Arbor's
[Commuter Challenge](https://challenge.getdowntown.org/)?
Do you want to form teams within your organization?
The project helps you set up a website
to allow your teams to see how they fare against each other.
Just record your team's info,
build the docker image,
and run it on a server.

**WARNING**:
This is was designed for use within an organization,
on a secure network.
Expose your docker image or deployed server
to the world at your own risk.
See the _Comments and notes_ section below.

**NOTE: OMG It's so slow!!!**
Yes, it's slow to load.
Be patient.
There's no API for Commuter Challenge data,
so we get your organization's data
by crawling the site with a headless browser.

## Setup

1. Define a user to access Commuter Challenge site
1. Define your organization and teams
1. Build the docker image
1. Run the docker image on a host

## Defining a user with access

You'll need to define a user to access the Commuter Challenge website.
You can do this by defining these environment variables:

```shell
export COMMMUTER_EMAIL=***
export COMMMUTER_PASSWORD=***
```

You might just want to create a special user to do that.

## Defining your organization and teams

We need some more info to allow the program to identify
members of your organization and
figure out which team they're in.

You'll want to put your own data into this file:

```shell
data/organization.json
```

Here's some sample data:

```json
{
    "name": "Org Name",
    "teams": {
        "First Team Name": [
            "Some user",
            "Another user"
        ],
        "Second Team Name": [
            "Some user",
            "Another user"
        ]
    }
}
```

## Building and running the website

You'll need to install [docker](https://www.docker.com/)
to build and run the site with the scripts provided.

Here are a few handy commands:

```shell
bin/build.sh
bin/run.sh
bin/stop.sh
```

- `build.sh`: Builds a docker image of the site.
Do this once you've defined your data.
- `run.sh`: Runs the website.
(Make sure port 80 is available.)
- `stop.sh` Stops the site if it's running
and removes the container to avoid conflicts.

This makes deploying the site very easy
once you've defined your data.

**WARNING**:
Don't make the docker image publicly accessible
because it contains your login info.

## Customization

There's a lot of room for improvement and customization.
For instance, you can modify the we scores are calculated
in `util/organization.py:score_function`.

Feel free to play around.

## Debugging

There's some debug code left around for your convenience.

The `Dockerfile` has a line commented out that'll set flask to debug mode.

`server/ccc_server.py` has some commented test code
that allows you to experiment without fetching data
from the real website every time.

## Comments and notes

This project was hacked together in a day.
It's not production-ready,
so don't deploy it publicly.

Although the sensitive login info isn't too out in the open,
it's accessible through the docker image.
So don't make that image publicly accessible.

And yes, it's super slow to load.
That's because without an API for Commuter Challenge data,
we crawl the site with a headless browser.
Not great, but it works.
