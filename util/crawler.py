from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException

from data import login_user

SIGNIN_PAGE = 'https://challenge.getdowntown.org/sign-in'
ALL_USERS_LEADERBOARD_PAGE = 'https://challenge.getdowntown.org/leaderboard/users'

def get_all_user_info():
    opts = Options()
    opts.headless = True
    browser = Firefox(options=opts)
    browser.get(SIGNIN_PAGE)
    sign_in(browser)
    browser.get(ALL_USERS_LEADERBOARD_PAGE)

    users = []
    users.extend(get_users_on_page(browser))
    while is_next_page(browser):
        go_to_next_page(browser)
        users.extend(get_users_on_page(browser))

    logout(browser)
    return users

def sign_in(browser):
    email_input = browser.find_element_by_xpath("//input[@name='email']")
    email_input.send_keys(login_user['email'])
    password_input = browser.find_element_by_xpath("//input[@name='password']")
    password_input.send_keys(login_user['password'])
    submit_button = browser.find_element_by_xpath("//button[@type='submit']")
    submit_button.click()

def get_users_on_page(browser):
    users_table = browser.find_element_by_xpath("//table[@class='table table-hover']/tbody")
    user_rows = users_table.find_elements_by_tag_name('tr')
    users = []
    for user_row in user_rows:
        user_info = user_row.find_elements_by_tag_name('td')
        users.append({
            'name': user_info[0].text,
            'organization': user_info[1].text,
            'trips': int(user_info[2].text),
        })
    return users

def is_next_page(browser):
    try:
        next_page_link = browser.find_element_by_xpath("//a[@rel='next']")
    except NoSuchElementException:
        return False
    return True

def go_to_next_page(browser):
    next_page_link = browser.find_element_by_xpath("//a[@rel='next']")
    next_page_link.click()

def logout(browser):
    logout_link = browser.find_element_by_link_text('Logout')
    logout_link.click()
