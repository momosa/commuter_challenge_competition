from util.crawler import get_all_user_info
from data import organization

from statistics import mean

def get_org_stats():
    """
    Get the stats for your organization

    Expect the user info to have the form:

        [
            {
                'name': 'User Name',
                'organization': 'Org Name',
                'trips': 42,
            },
        ]

    Expect organization info to have the form:

        {
            'name': 'Org Name',
            'teams': {
                'Team Name': [
                    'User Name',
                ]
            }
        }

    Generates output of the form:

        {
            'name': 'Org Name',
            'score': 42
            'teams': {
                'Team Name': {
                    'score': 42,
                }
            }
        }

    """
    all_user_info = get_all_user_info()
    org_user_info = get_org_user_info(all_user_info, organization['name'])
    org_stats = {
        'name': organization['name'],
        'teams': None,
    }
    org_stats['teams'] = {
        team_name: {
            'score': calculate_team_score(org_user_info, team_members)
        }
        for team_name, team_members in organization['teams'].items()
    }
    org_stats['score'] = score_function((
        org_stats['teams'][team_name]['score']
        for team_name in org_stats['teams']
    ))
    return org_stats

def score_function(score_list):
    """
        Adjust this to change how scores are calculated from a list
    """
    return mean(score_list)

def calculate_team_score(org_user_info, team_members):
    team_info = [
        user_info
        for user_info in org_user_info
        if user_info['name'].strip() in team_members
    ]
    team_trips = [
        int(user_info['trips'])
        for user_info in team_info
    ]
    return score_function(team_trips)

def get_org_user_info(all_user_info, org_name):
    org_user_info = [
        {
            'name': user_info['name'],
            'trips': user_info['trips'],
        }
        for user_info in all_user_info
        if user_is_in_org(user_info, org_name)
    ]
    return org_user_info

def user_is_in_org(user_info, org_name):
    return org_name.lower() in user_info.get('organization', '').lower()
