from util import get_org_stats
from data import organization
import json

from flask import Flask, jsonify, render_template
app = Flask(__name__)

# Useful for experimenting
TEST_STATS = {
    'name': 'Org Name',
    'score': 42.0,
    'teams': {
        'Team One': {'score': 23.3},
        'Team Two': {'score': 14.8},
        'Team Three': {'score': 39.0},
    },
}

@app.route('/')
def home():
    org_stats = get_org_stats()
    #org_stats = TEST_STATS
    return render_template(
        'index.html',
        org_name=organization['name'],
        stats=org_stats,
    )

@app.route('/api/org_stats/')
def org_stats():
    return jsonify(get_org_stats())
