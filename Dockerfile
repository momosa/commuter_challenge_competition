FROM python:3.7

# Install applications
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qy update && \
  apt-get install -qqy firefox-esr

# Setup the project dir
RUN mkdir /commuter_challenge_competition
WORKDIR /commuter_challenge_competition/

# Install the firefox webdriver
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
RUN tar -xvzf geckodriver*
ENV PATH "$PATH:/commuter_challenge_competition"

# Copy the project into the image
COPY . /commuter_challenge_competition/

# Install python requirements
RUN pip install -r requirements.txt

# For debugging
#ENV FLASK_ENV development

# Expect these variables from the environment
ARG COMMUTER_EMAIL
ARG COMMUTER_PASSWORD

# Run the server
ENV COMMUTER_EMAIL $COMMUTER_EMAIL
ENV COMMUTER_PASSWORD $COMMUTER_PASSWORD
ENV FLASK_APP ccc_server.py
ENTRYPOINT /bin/bash -ce "cd server && flask run --host 0.0.0.0"
