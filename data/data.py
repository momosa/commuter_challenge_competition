import os
import inspect
import json
import copy

DATA_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

def get_organization():
    org = _get_data_from_json('organization')
    assert 'name' in org
    assert 'teams' in org
    return org

def get_login_user():
    user = {
        'email': os.environ.get('COMMUTER_EMAIL'),
        'password': os.environ.get('COMMUTER_PASSWORD'),
    }
    if not all((user['email'], user['password'])):
        raise KeyError(
            f'The environment should contain COMMUTER_EMAIL and COMMUTER_PASSWORD.'
        )
    return user

def _get_data_from_json(data_name):
    filename = os.path.join(DATA_DIR, f'{data_name}.json')
    with open(filename) as json_data:
        data = json.loads(json_data.read())
    return data
